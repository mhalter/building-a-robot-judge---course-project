"""
Create, train and save embedding layer
"""

import logging
import numpy
from gensim.models import Word2Vec
import Global
from utils import get_documents
from iterables import SentencesIterable


class EmbeddingModel:

    def __init__(self):
        """Create word embedding model for documents in DATA_PATH"""
        logging.getLogger('smart_open').setLevel(logging.ERROR)
        self.documents = get_documents(
            path=Global.DATA_PATH
        )
        logging.info('Dataset loaded (dataset size: {})'
                     .format(len(self.documents)))

    def get_word_vectors(self, field: int):
        """Get word vectors for field and save it at
        Global.WORD_VECTOR_FILE_PATH[field]
        """
        sentences_iterable = SentencesIterable(
            documents=self.documents,
            field=field
        )
        logging.info('Creating word vectors ({})'
                     .format(Global.WORD_VECTOR_FILE_PATH[field]))
        word2vec = Word2Vec(sentences=sentences_iterable,
                            size=Global.HIDDEN_DIMENSIONS,
                            window=Global.CONTEXT_WINDOW,
                            alpha=Global.W2V_ALPHA,
                            workers=Global.WORKERS,
                            sg=Global.SKIPGRAM,
                            negative=Global.NEGATIVE_SAMPLES,
                            ns_exponent=Global.NS_EXPONENT,
                            sample=Global.SAMPLE)
        word2vec.wv.add(
            '_unknown_',
            numpy.random.rand(Global.HIDDEN_DIMENSIONS)
        )
        if field != 0:
            word2vec.wv.add(
                '_bos_',
                numpy.random.rand(Global.HIDDEN_DIMENSIONS)
            )
        logging.info('{} vocabulary size: {}'
                     .format(Global.WORD_VECTOR_FILE_PATH[field],
                             len(word2vec.wv.vocab)))
        word2vec.wv.save(Global.WORD_VECTOR_FILE_PATH[field])
        