from gensim.models import KeyedVectors
from keras.layers import Input, LSTM, Dense, Concatenate, TimeDistributed
from keras.optimizers import SGD
from keras.models import Model
from keras.utils import plot_model
from sklearn.model_selection import train_test_split
from AttentionLayer import AttentionLayer
import tensorflow
import numpy
import math
import logging
from WordGenerator import WordGenerator
from BatchGenerator import batchGenerator
from LoadDataset import loadDataset
import Global


trainDocs, testDocs = train_test_split(loadDataset(path=Global.DATAPATH),train_size=0.2)

logging.info('Dataset loaded (training dataset size: {}, testing dataset size: {})'
                .format(len(trainDocs), len(testDocs)))

tensorflow.logging.set_verbosity(tensorflow.logging.ERROR)


inputWV = KeyedVectors.load(Global.OPINIONMODELPATH)
targetWV = KeyedVectors.load(Global.CASESUMMARYMODELPATH)

logging.info('Wordvectors loaded (input vocabulary size: {}, target vocabulary size: {})'
                .format(len(inputWV.vocab), len(targetWV.vocab)))


encoderEmbeddingInputs = Input(shape=(None,))
encoderEmbedding = inputWV.get_keras_embedding(train_embeddings=False)
encoderInputs = encoderEmbedding(encoderEmbeddingInputs)

encoder = LSTM(units=Global.WVDIM, return_sequences=True, return_state=True)
encoderOutputs, stateH, stateC = encoder(encoderInputs)
encoderStates = [stateH, stateC]

decoderEmbeddingInputs = Input(shape=(None,))
decoderEmbedding = targetWV.get_keras_embedding(train_embeddings=False)
decoderInputs = decoderEmbedding(decoderEmbeddingInputs)
decoder = LSTM(units=Global.WVDIM, return_sequences=True, return_state=True)
decoderOutputs, _, _ = decoder(decoderInputs, initial_state=encoderStates)

attentionLayer = AttentionLayer()
attentionOutput = attentionLayer([encoderOutputs, decoderOutputs])

concatenateLayer = Concatenate(axis=-1)

decoderConcOutput = concatenateLayer([decoderOutputs, attentionOutput])

print('decoderConcOutput shape: {}'.format(decoderConcOutput.shape))

decoderDense = Dense(units=len(targetWV.vocab))

decoderPrediction = decoderDense(decoderConcOutput)


trainGenerator = batchGenerator(batchSize=Global.EDBATCHSIZE,
                                targetVocabSize=len(targetWV.vocab),
                                inputWordVectors=inputWV,
                                targetWordVectors=targetWV,
                                docs=trainDocs)

testGenerator = batchGenerator(batchSize=Global.EDBATCHSIZE,
                                targetVocabSize=len(targetWV.vocab),
                                inputWordVectors=inputWV,
                                targetWordVectors=targetWV,
                                docs=testDocs)

del inputWV, targetWV

model = Model(inputs=[encoderEmbeddingInputs, decoderEmbeddingInputs], outputs=decoderPrediction)

opt = SGD(lr=1e-2, momentum=0.9, decay=1e-2 / Global.EDEPOCHS)

model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=["accuracy"])

model.summary()

model.fit_generator(trainGenerator,
                    steps_per_epoch=math.ceil(len(trainDocs)/Global.EDBATCHSIZE),
                    validation_data=testGenerator,
                    validation_steps=math.ceil(len(testDocs)/Global.EDBATCHSIZE),
                    epochs=Global.EDEPOCHS)