from keras import backend
from keras.layers import Layer, Dense, Activation, Multiply
import Global

import sys

class AttentionLayer(Layer):

    def __init__(self, **kwargs):
        
        super(AttentionLayer, self).__init__(**kwargs)

    def build(self, inputShape):

        self.attentionWeightLayer = Dense(units=inputShape[0][2])

        self.scoreActivation = Activation(activation='softmax')

        self.muliplicationLayer = Multiply()


        super(AttentionLayer, self).build(inputShape)

    def call(self, inputs):

        assert type(inputs) == list

        encoderOutputs, decoderOutputs = inputs

        def decoderStepFunction(inputs, states):

            _, scores, _ = backend.rnn(encoderScoreStepFunction, encoderOutputs, initial_states=[inputs])

            activatedScores = self.scoreActivation(scores)

            repeatedActivatedScores = backend.repeat_elements(activatedScores, rep=encoderOutputs.shape[2], axis=2)

            attentionVector = self.muliplicationLayer([encoderOutputs, repeatedActivatedScores])

            contextVector = backend.sum(attentionVector, axis=2, keepdims=False)

            return contextVector, states

        def encoderScoreStepFunction(inputs, states):

            weightDotEncoderState = self.attentionWeightLayer(inputs)

            score = backend.batch_dot(states[0], weightDotEncoderState, axes=(1))

            return score, states

        _, contextVector, _ = backend.rnn(decoderStepFunction, decoderOutputs, initial_states=[])

        return contextVector

    def compute_output_shape(self, inputShape):
    
        return (inputShape[1][0],inputShape[1][1], inputShape[0][1])