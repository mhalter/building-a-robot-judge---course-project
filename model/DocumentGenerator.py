import os
from zipfile import ZipFile
import json
from pandas import DataFrame
from nltk.tokenize import sent_tokenize, word_tokenize
import Global

class DocumentGenerator:
    """Iterator to load a new document from disk"""
    
    docs = None
    field = None
    tokenizer = None

    def __init__(self, docs, field, tokenizer):

        self.docs = docs.iterrows()
        self.field = field
        self.tokenizer = tokenizer

    def __iter__(self):

        return self

    def __next__(self):
        
        doc = next(self.docs)
        jsonObject = json.loads(ZipFile(doc[1]['sourcefile'],mode='r').read(doc[1]['docname']))
        
        if self.field == 0:
            rawText = jsonObject['opinion']
        elif self.field == 1:
            rawText = jsonObject['case_summary']['overview']
        elif self.field == 2:
            rawText = jsonObject['case_summary']['outcome']
        else:
            #rawText = jsonObject['headnotes']
            rawText=''

        rawText = '_bos_ ' + rawText + ' _eos_'

        if self.tokenizer == 0:
            return word_tokenize(rawText)
        else:
            return [word_tokenize(sent) for sent in sent_tokenize(rawText)]