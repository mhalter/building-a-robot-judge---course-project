import os, json
from zipfile import ZipFile
from pandas import DataFrame

def loadDataset(path):
    """create a list of all available and useable cases"""

    zFileList = []

    for r, _, f in os.walk(path):
        for file in f:
            if '.zip' in file:
                zFileList.append(os.path.join(r,file))

    docList = []
    sourceFileList = []

    for f in zFileList:
        zf = ZipFile(f, mode='r')

        for name in zf.namelist():
            if '.json' in name and not '__MACOSX' in name:
                doc = json.loads(zf.read(name))
                if not doc['opinion'] == '' and not doc['case_summary'] == '' and not doc['headnotes'] == '':
                    docList.append(name)
                    sourceFileList.append(f)

    return DataFrame(data={'docname': docList, 'sourcefile': sourceFileList})