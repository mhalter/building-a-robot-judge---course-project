"""Attention layer implementation"""

import math
from keras import backend
from keras.layers import Layer
import numpy
import Global


class AttentionLayer(Layer):

    def __init__(self, **kwargs):
        super(AttentionLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        self.attention_weights = self.add_weight(
            name='attention_weights',
            shape=(input_shape[0][2], input_shape[0][2]),
            initializer='uniform',
            trainable=True
        )
        super(AttentionLayer, self).build(input_shape)

    def call(self, inputs):
        assert type(inputs) == list
        encoder_sequence, decoder_sequence = inputs

        def decoder_step_function(inputs, states):
            _, scores, _ = backend.rnn(encoder_step_function,
                                       encoder_sequence,
                                       initial_states=[inputs])
            activated_scores = backend.softmax(scores, axis=-1)
            repeated_scores = backend.repeat_elements(
                activated_scores,
                Global.HIDDEN_DIMENSIONS,
                axis=2
            )
            attention_vector = repeated_scores * encoder_sequence
            context_vector = backend.sum(attention_vector,
                                         axis=1,
                                         keepdims=False)
            return context_vector, states

        def encoder_step_function(inputs, states):
            W_dot_encoder_state = backend.dot(inputs, self.attention_weights)
            score = backend.batch_dot(states[0], W_dot_encoder_state, axes=(-1))
            return score, states

        _, context_vector, _ = backend.rnn(
            decoder_step_function,
            decoder_sequence,
            initial_states=[]
        )
        return context_vector

    def compute_output_shape(self, input_shape):
        return (input_shape[1][0], input_shape[1][1], input_shape[1][2])


class MultiHeadAttention(Layer):

    def __init__(self, **kwargs):
        super(MultiHeadAttention, self).__init__(**kwargs)

    def build(self, input_shape):
        self.w_q = self.add_weight(
            name='w_q',
            shape=(Global.HIDDEN_DIMENSIONS,
                   Global.HIDDEN_DIMENSIONS/Global.MULTIHEAD_DIMENSIONS,
                   Global.MULTIHEAD_DIMENSIONS),
            initializer='uniform',
            trainable=True
        )
        self.w_k = self.add_weight(
            name='w_k',
            shape=(Global.HIDDEN_DIMENSIONS,
                   Global.HIDDEN_DIMENSIONS/Global.MULTIHEAD_DIMENSIONS,
                   Global.MULTIHEAD_DIMENSIONS),
            initializer='uniform',
            trainable=True
        )
        self.w_v = self.add_weight(
            name='w_v',
            shape=(Global.HIDDEN_DIMENSIONS,
                   Global.HIDDEN_DIMENSIONS/Global.MULTIHEAD_DIMENSIONS,
                   Global.MULTIHEAD_DIMENSIONS),
            initializer='uniform',
            trainable=True
        )
        self.w = self.add_weight(
            name='w',
            shape=(Global.HIDDEN_DIMENSIONS,
                   Global.HIDDEN_DIMENSIONS),
            initializer='uniform',
            trainable=True
        )
        super(MultiHeadAttention, self).build(input_shape)

    def call(self, inputs):
        assert type(inputs) == list
        q, k, v = inputs
        q_w_q = backend.batch_dot(q, self.w_q)
        k_w_k = backend.batch_dot(k, self.w_k)
        v_w_v = backend.batch_dot(v, self.w_v)
        k_w_k_T = backend.transpose(k_w_k)
        q_k = backend.batch_dot(q_w_q, k_w_k_T)
        intermediate = q_k / math.sqrt(
            Global.HIDDEN_DIMENSIONS/Global.MULTIHEAD_DIMENSIONS
        )
        softmax = backend.softmax(intermediate)
        single_attention = backend.batch_dot(v_w_v, softmax)
        flattened = backend.flatten(single_attention)
        return backend.batch_dot(flattened, self.w)

    def compute_output_shape(self, input_shape):
        return input_shape[0]


class FeedForward(Layer):

    def __init__(self, **kwargs):
        super(FeedForward, self).__init__(**kwargs)

    def build(self, input_shape):
        self.weights_1 = self.add_weight(
            name='weights_1',
            shape=(input_shape[0][2], input_shape[0][2]),
            initializer='uniform',
            trainable=True
        )
        self.weights_2 = self.add_weight(
            name='weights_2',
            shape=(input_shape[0][2], input_shape[0][2]),
            initializer='uniform',
            trainable=True
        )
        self.bias_1 = self.add_weight(
            name='bias_1',
            shape=(input_shape[0][2]),
            initializer='uniform',
            trainable=True
        )
        self.bias_2 = self.add_weight(
            name='bias_2',
            shape=(input_shape[0][2]),
            initializer='uniform',
            trainable=True
        )
        super(FeedForward, self).build(input_shape)

    def call(self, inputs):
        assert type(inputs) == list
        intermediate = backend.batch_dot(inputs, self.weights_1)
        intermediate = backend.bias_add(intermediate, self.bias_1)
        intermediate = backend.maximum(
            backend.zeros(inputs.shape[0][2]),
            intermediate
        )
        intermediate = backend.batch_dot(intermediate, self.weights_2)
        return backend.bias_add(intermediate, self.bias_2)

    def compute_output_shape(self, input_shape):
        return input_shape[0]


class PositionalEncoding(Layer):

    def __init__(self, **kwargs):
        super(PositionalEncoding, self).__init__(**kwargs)

    def build(self, input_shape):
        super(PositionalEncoding, self).build(input_shape)

    def call(self, inputs):
        assert type(inputs) == list
        positional_encoding = numpy.zeros(shape=(
            inputs.shape[1],
            Global.HIDDEN_DIMENSIONS
        ))
        for pos in range(inputs.shape[1]):
            for i in range(Global.HIDDEN_DIMENSIONS):
                positional_encoding[pos][i] = math.sin(
                    pos / 10000**(2*i/Global.HIDDEN_DIMENSIONS)
                )
        return inputs + positional_encoding

    def compute_output_shape(self, input_shape):
        return input_shape[0]


class AddAndNorm(Layer):

    def __init__(self, **kwargs):
        super(AddAndNorm, self).__init__(**kwargs)

    def build(self, input_shape):
        self.gain = self.add_weight(
            name='gain',
            shape=(Global.HIDDEN_DIMENSIONS),
            initializer='uniform',
            trainable=True
        )
        self.bias = self.add_weight(
            name='bias',
            shape=(Global.HIDDEN_DIMENSIONS),
            initializer='uniform',
            trainable=True
        )
        super(AddAndNorm, self).build(input_shape)

    def call(self, inputs):
        assert type(inputs) == list
        add = inputs[0] + inputs[1]
        mu = backend.sum(add, axis=-1)/Global.HIDDEN_DIMENSIONS
        sig = (backend.sum((add-mu)**2)/Global.HIDDEN_DIMENSIONS)**(1/2)
        return (self.gain/sig)*(add-mu) + self.bias

    def compute_output_shape(self, input_shape):
        return input_shape[0]


class MaskLayer(Layer):

    def __init__(self, **kwargs):
        super(MaskLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        super(MaskLayer, self).build(input_shape)

    def call(self, inputs):
        assert type(inputs) == list
        return inputs

    def compute_output_shape(self, input_shape):
        return input_shape[0]