import numpy
from WordGenerator import WordGenerator
import Global

def batchGenerator(batchSize, targetVocabSize, inputWordVectors, targetWordVectors, docs):

    inputGenerator = WordGenerator(docs=docs, field=Global.OPINION)
    targetGenerator = WordGenerator(docs=docs, field=Global.CASESUMMARY)
    targetWV = targetWordVectors
    inputWV = inputWordVectors

    while True:
        inputTexts = []
        targetTexts = []

        while len(inputTexts) < batchSize:
            try:
                inputTexts.append(next(inputGenerator))
                targetTexts.append(next(targetGenerator))
            except StopIteration:
                inputGenerator = WordGenerator(docs=docs, field=Global.OPINION)
                targetGenerator = WordGenerator(docs=docs, field=Global.CASESUMMARY)

        maxInputText = max([len(txt) for txt in inputTexts])
        maxTargetText = max([len(txt) for txt in targetTexts])

        encoderInputData = numpy.zeros((len(inputTexts), maxInputText), dtype='float32')
        decoderInputData = numpy.zeros((len(targetTexts), maxTargetText), dtype='float32')
        decoderTargetData = numpy.zeros((len(targetTexts), maxTargetText, len(targetWV.vocab)), dtype='float32')

        for i, (inputText, targetText) in enumerate(zip(inputTexts, targetTexts)):

            for j, word in enumerate(inputText):
                try:
                    encoderInputData[i, j] = inputWV.vocab[word].index
                except KeyError:
                    encoderInputData[i, j] = inputWV.vocab['_unknown_'].index

            for j, word in enumerate(targetText):
                try:
                    decoderInputData[i, j] = targetWV.vocab[word].index
                except KeyError:
                    decoderInputData[i, j] = targetWV.vocab['_unknown_'].index

                if j > 0:
                    try:
                        decoderTargetData[i, j - 1, targetWV.vocab[word].index]  = 1
                    except KeyError:
                        decoderTargetData[i, j - 1, targetWV.vocab['_unknown_'].index] = 1
        
        yield [encoderInputData, decoderInputData], decoderTargetData