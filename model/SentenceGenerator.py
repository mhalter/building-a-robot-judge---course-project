from DocumentGenerator import DocumentGenerator
from zipfile import ZipFile
import Global

class SentenceGenerator:
    """Iterator to read the sentences from disk on the fly"""

    docGenerator = None
    sentences = None

    def __init__(self, docs, field):
        self.docGenerator = iter(DocumentGenerator(docs=docs, field=field, tokenizer=Global.SENTTOK))
        self.sentences = iter(next(self.docGenerator))

    def __iter__(self):
    
        return self
            
    def __next__(self):
        try:
            return next(self.sentences)
        except StopIteration:
            self.sentences = iter(next(self.docGenerator))
            return next(self.sentences)