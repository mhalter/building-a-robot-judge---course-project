#!/usr/bin/env python

import logging
logging.basicConfig(level=logging.DEBUG,
                    filename='model.log',
                    filemode='w')

from sklearn.model_selection import train_test_split
from embeddingmodel import EmbeddingModel
from encoderdecodermodel import LSTMModel, GRUModel
from utils import get_documents
from iterables import SentencesIterable
import Global

#emmodel = EmbeddingModel()
#emmodel.get_word_vectors(1)
#emmodel.get_word_vectors(0)
#del emmodel

edmodel = GRUModel(1)
documents = get_documents(Global.DATA_PATH)
train_documents, test_documents = train_test_split(
    documents, train_size=Global.SET_SPLIT
)
edmodel.train_model(edmodel.model, train_documents, test_documents)
edmodel.predict(test_documents.sample())