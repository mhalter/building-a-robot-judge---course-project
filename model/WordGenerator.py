from DocumentGenerator import DocumentGenerator
from zipfile import ZipFile
import Global

class WordGenerator:
    """Iterator to read the sentences from disk on the fly"""

    docGenerator = None
    text = None

    def __init__(self, docs, field):
        self.docGenerator = iter(DocumentGenerator(docs=docs, field=field, tokenizer=Global.WORDTOK))

    def __iter__(self):
    
        return self
            
    def __next__(self):
        
        return next(self.docGenerator)