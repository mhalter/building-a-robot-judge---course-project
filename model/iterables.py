"""
Read documents on the fly from disk
"""

import json
import logging
from zipfile import ZipFile
from nltk.tokenize import word_tokenize, sent_tokenize


class DocumentsIterator:
    """Iterator for documents"""

    def __init__(
            self,
            documents,
            field: int,
            tokenizer: int = 0):
        """Document iterator for documents

        tokenizer:
            0: tokenize by word
            1: tokenize by sentence
        """
        self.documents = documents.iterrows()
        self.field = field
        self.tokenizer = tokenizer

    def __iter__(self):
        return self

    def __next__(self):
        doc = next(self.documents)
        zip_file = ZipFile(doc[1]['source_file'], mode='r')
        jsonObject = json.loads(zip_file.read(doc[1]['document_name']))
        
        if self.field == 0:
            raw_text = jsonObject['opinion']
        elif self.field == 1:
            raw_text = jsonObject['case_summary']['overview']
            raw_text += ' _eos_'
        elif self.field == 2:
            raw_text = jsonObject['case_summary']['outcome']
            raw_text += ' _eos_'
        elif self.field == 3:
            for hn in jsonObject['headnotes']:
                raw_text = ' _hn_ '.join([raw_text,
                                               jsonObject['headnotes'][hn]])

        else:
            raw_text = ''
            logging.warning('Invalid field.')

        clean_text = raw_text #ToDo Cleaning
        return clean_text


class SentencesIterator:
    """Iterator for sentences"""

    def __init__(
            self,
            documents,
            field: int):
        """Sentence iterator for documents

        field:
            0: ['opinion']
            1: ['case_summary']['overview']
            2: ['case_summary']['outcome']
            3: ['headnotes']
        """
        self.document_iterator = iter(DocumentsIterator(
                                            documents=documents,
                                            field=field,
                                            tokenizer=1))
        self.field = field
        self.sentences = iter([word_tokenize(sent) for sent in sent_tokenize(
            next(self.document_iterator)
        )])

    def __iter__(self):
        return self

    def __next__(self):
        try:
            sentence = next(self.sentences)
        except StopIteration:
            self.sentences = iter([
                word_tokenize(sent) for sent in sent_tokenize(
                    next(self.document_iterator)
                )
            ])
            sentence = next(self.sentences)
        logging.debug('Sentence provided: {}'
            .format(' '.join(sentence).encode('utf-8')))
        return sentence


class SentencesIterable:

    def __init__(self, documents, field):
        self.documents = documents
        self.field = field

    def __iter__(self):
        return iter(SentencesIterator(documents=self.documents,
                                      field=self.field))


class WordsIterator:

    def __init__(self, documents, field):
        self.document_iterator = iter(DocumentsIterator(
                                            documents=documents,
                                            field=field,
                                            tokenizer=1))

    def __iter__(self):
        return self
    
    def __next__(self):
        text = next(self.document_iterator)
        logging.debug('Text provided: {}'.format(
            text
        ))
        return word_tokenize(text)