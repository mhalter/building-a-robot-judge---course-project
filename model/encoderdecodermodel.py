"""Encoder-Decoder Models
LSTM, GRU, Transformer
"""

import logging
import math
import tensorflow
import numpy
from gensim.models import KeyedVectors
from keras.models import Model, load_model
from keras.optimizers import SGD
from keras.layers import Input, LSTM, GRU, Dense
from keras.layers import Concatenate, TimeDistributed
from utils import get_documents, generate_batch, get_single_file_batch
from attention import AttentionLayer, MultiHeadAttention, PositionalEncoding
from attention import FeedForward, AddAndNorm, MaskLayer
import Global


class EncoderDecoderModel:

    def __init__(self, field):
        self.model = None
        self.encoder_model = None
        self.decoder_model = None
        tensorflow.compat.v1.logging.set_verbosity(
            tensorflow.compat.v1.logging.ERROR
        )
        self.source_wv = KeyedVectors.load(Global.WORD_VECTOR_FILE_PATH[0])
        logging.info('{} loaded (vocabulary size: {}'.format(
            Global.WORD_VECTOR_FILE_PATH[0],
            len(self.source_wv.vocab)
        ))
        self.target_wv = KeyedVectors.load(Global.WORD_VECTOR_FILE_PATH[field])
        logging.info('{} loaded (vocabulary size: {}'.format(
            Global.WORD_VECTOR_FILE_PATH[field],
            len(self.target_wv.vocab)
        ))
        self.field = field

    def train_model(self, model, train_documents, test_documents):
        """Train Model"""
        logging.info(
            'Training dataset size: {}'.format(len(train_documents))
        )
        logging.info(
            'Testing dataset size: {}'.format(len(test_documents))
        )
        train_generator = generate_batch(
            batch_size=Global.ENCODER_DECODER_BATCH_SIZE,
            target_wv=self.target_wv,
            target_field=self.field,
            source_wv=self.source_wv,
            documents=train_documents
        )
        test_generator = generate_batch(
            batch_size=Global.ENCODER_DECODER_BATCH_SIZE,
            target_wv=self.target_wv,
            target_field=self.field,
            source_wv=self.source_wv,
            documents=test_documents
        )
        model.fit_generator(
            train_generator,
            steps_per_epoch=math.ceil(
                len(train_documents)/Global.ENCODER_DECODER_BATCH_SIZE
            ),
            validation_data=test_generator,
            validation_steps=math.ceil(
                len(test_documents)/Global.ENCODER_DECODER_BATCH_SIZE
            ),
            epochs=Global.ENCODER_DECODER_EPOCHS
        )

    def save_model(self, fn_1, fn_2, fn_3):
        self.model.save(fn_1)
        self.encoder_model.save(fn_2)
        self.decoder_model.save(fn_3)

    def load_model(self, fn_1, fn_2):
        self.encoder_model = load_model(fn_1)
        self.decoder_model = load_model(fn_2)
        


class LSTMModel(EncoderDecoderModel):

    def __init__(self, field: int):
        """Create encoder-decoder model for field
        field:
            1: ['case_summary']['overview']
            2: ['case_summary']['outcome']
            3: ['headnotes']
        """
        super(LSTMModel, self).__init__(field)
        self.build_model()

    def build_model(self):
        """Build and Fit model"""
        source_embedding_input = Input(shape=(None,))
        source_embedding = self.source_wv.get_keras_embedding(
            train_embeddings=False
        )
        source_embedded = source_embedding(source_embedding_input)
        lstm_encoder = LSTM(units=Global.HIDDEN_DIMENSIONS,
                            return_sequences=True,
                            return_state=True)
        encoder_output, state_h, state_c = lstm_encoder(source_embedded)
        encoder_states = [state_h, state_c]
        
        target_embedding_input = Input(
            shape=(None,)
        )
        target_embedding = self.target_wv.get_keras_embedding(
            train_embeddings=False
        )
        target_embedded = target_embedding(target_embedding_input)
        lstm_decoder = LSTM(units=Global.HIDDEN_DIMENSIONS,
                            return_sequences=True,
                            return_state=True)
        decoder_output, _, _ = lstm_decoder(target_embedded,
                                            initial_state=encoder_states)

        attention_layer = AttentionLayer()
        attention_output = attention_layer([encoder_output, decoder_output])
        concatenate_layer = Concatenate(axis=-1)
        concatenated_output = concatenate_layer([
            decoder_output, attention_output
        ])
        decoder_prediction_layer = TimeDistributed(Dense(
            units=len(self.target_wv.vocab))
        )
        decoder_prediction = decoder_prediction_layer(concatenated_output)
        self.model = Model(
            inputs=[source_embedding_input, target_embedding_input],
            outputs=decoder_prediction
        )
        optimizer = SGD(lr=1e-2,
                        momentum=0.9,
                        decay=1e-2/Global.ENCODER_DECODER_EPOCHS)
        self.model.compile(optimizer=optimizer,
                           loss='categorical_crossentropy',
                           metrics=['accuracy'])
        logger = logging.getLogger(__name__)
        self.model.summary(print_fn=logger.info)

        inf_source_input = Input(shape=(None,))
        inf_enc_in = source_embedding(inf_source_input)
        inf_enc_out, inf_enc_s_h, inf_enc_s_c = lstm_encoder(inf_enc_in)
        self.encoder_model = Model(inputs=inf_source_input,
                                   outputs=[
                                       inf_enc_out,
                                       inf_enc_s_h,
                                       inf_enc_s_c
                                   ])
        self.encoder_model.summary(print_fn=logger.info)
        inf_att_in = Input(shape=(
            None,
            Global.HIDDEN_DIMENSIONS
        ))
        inf_dec_initial_s_h = Input(shape=(
            Global.HIDDEN_DIMENSIONS,
        ))
        inf_dec_initial_s_c = Input(shape=(
            Global.HIDDEN_DIMENSIONS,
        ))
        inf_target_input = Input(shape=(1,))
        inf_dec_in = target_embedding(inf_target_input)
        inf_dec_out, inf_dec_s_h, inf_dec_s_c = lstm_decoder(
            inf_dec_in,
            initial_state=[inf_dec_initial_s_h, inf_dec_initial_s_c]
        )

        inf_attention_out = attention_layer([
            inf_att_in,
            inf_dec_out
        ])
        inf_concatenate = Concatenate(axis=-1)
        inf_dec_conc = inf_concatenate([
            inf_dec_out,
            inf_attention_out
        ])
        inf_dense = Dense(units=len(self.target_wv.vocab))
        inf_prediction = inf_dense(inf_dec_conc)
        self.decoder_model = Model(
            inputs=[
                inf_att_in,
                inf_dec_initial_s_h,
                inf_dec_initial_s_c,
                inf_target_input
            ],
            outputs=[
                inf_prediction,
                inf_dec_s_h,
                inf_dec_s_c
            ]
        )
        self.decoder_model.summary(print_fn=logger.info)
    
    def predict(self, document):
        """Make prediction for documents"""
        source_sequence, target_text, source_text = get_single_file_batch(
            self.target_wv,
            self.field,
            self.source_wv,
            document
        )
        logging.info('Making prediction for:{}'.format(source_text))
        enc_out, enc_last_s_h, enc_last_s_c = self.encoder_model.predict(
            source_sequence
        )
        target_prediction = ''
        #attention_scores = []
        dec_in = numpy.zeros((1,1), dtype='int')
        dec_in[0][0] = self.target_wv.vocab['_bos_'].index
        dec_s_h = enc_last_s_h[-1].reshape(
            1,
            Global.HIDDEN_DIMENSIONS
        )
        dec_s_c = enc_last_s_c[-1].reshape(
            1,
            Global.HIDDEN_DIMENSIONS
        )
        enc_out = enc_out.reshape(
            1,
            len(enc_out),
            Global.HIDDEN_DIMENSIONS
        )
        for _ in range(Global.MAX_TARGET_LENGTH):
            dec_out, dec_s_h, dec_s_c = self.decoder_model.predict(
                [enc_out, dec_s_h, dec_s_c, dec_in]
            )
            logging.debug('Decoder prediction: {}\n\tfor decoder input: {}'
                          .format(dec_out, dec_in))
            dec_out = numpy.argmax(dec_out)
            word = self.target_wv.index2word[dec_out]
            dec_in[0][0] = dec_out
            if word == '_eos_':
                break
            target_prediction += word + ' '

        logging.info('Prediction: {}'.format(target_prediction))
        logging.info('Target: {}'.format(target_text))


class GRUModel(EncoderDecoderModel):

    def __init__(self, field: int):
        """Create encoder-decoder model for field
        field:
            1: ['case_summary']['overview']
            2: ['case_summary']['outcome']
            3: ['headnotes']
        """
        super(GRUModel, self).__init__(field)
        self.build_model()

    def build_model(self):
        """Build and Fit model"""
        source_embedding_input = Input(shape=(None,))
        source_embedding = self.source_wv.get_keras_embedding(
            train_embeddings=False
        )
        source_embedded = source_embedding(source_embedding_input)
        gru_encoder = GRU(units=Global.HIDDEN_DIMENSIONS,
                            return_sequences=True,
                            return_state=True)
        encoder_output, encoder_state = gru_encoder(source_embedded)
        
        target_embedding_input = Input(
            shape=(None,)
        )
        target_embedding = self.target_wv.get_keras_embedding(
            train_embeddings=False
        )
        target_embedded = target_embedding(target_embedding_input)
        gru_decoder = GRU(units=Global.HIDDEN_DIMENSIONS,
                            return_sequences=True,
                            return_state=True)
        decoder_output, _ = gru_decoder(target_embedded,
                                            initial_state=encoder_state)

        attention_layer = AttentionLayer()
        attention_output = attention_layer([encoder_output, decoder_output])
        concatenate_layer = Concatenate(axis=-1)
        concatenated_output = concatenate_layer([
            decoder_output, attention_output
        ])
        decoder_prediction_layer = TimeDistributed(Dense(
            units=len(self.target_wv.vocab))
        )
        decoder_prediction = decoder_prediction_layer(concatenated_output)
        self.model = Model(
            inputs=[source_embedding_input, target_embedding_input],
            outputs=decoder_prediction
        )
        optimizer = SGD(lr=1e-2,
                        momentum=0.9,
                        decay=1e-2/Global.ENCODER_DECODER_EPOCHS)
        self.model.compile(optimizer=optimizer,
                           loss='categorical_crossentropy',
                           metrics=['accuracy'])
        logger = logging.getLogger(__name__)
        self.model.summary(print_fn=logger.info)

        inf_source_input = Input(shape=(None,))
        inf_enc_in = source_embedding(inf_source_input)
        inf_enc_out, inf_enc_h = gru_encoder(inf_enc_in)
        self.encoder_model = Model(inputs=inf_source_input,
                                   outputs=[
                                       inf_enc_out,
                                       inf_enc_h
                                   ])
        self.encoder_model.summary(print_fn=logger.info)
        inf_att_in = Input(shape=(
            None,
            Global.HIDDEN_DIMENSIONS
        ))
        inf_dec_initial_h = Input(shape=(
            Global.HIDDEN_DIMENSIONS,
        ))
        inf_target_input = Input(shape=(1,))
        inf_dec_in = target_embedding(inf_target_input)
        inf_dec_out, inf_dec_h = gru_decoder(
            inf_dec_in,
            initial_state=inf_dec_initial_h
        )

        inf_attention_out = attention_layer([
            inf_att_in,
            inf_dec_out
        ])
        inf_concatenate = Concatenate(axis=-1)
        inf_dec_conc = inf_concatenate([
            inf_dec_out,
            inf_attention_out
        ])
        inf_dense = Dense(units=len(self.target_wv.vocab))
        inf_prediction = inf_dense(inf_dec_conc)
        self.decoder_model = Model(
            inputs=[
                inf_att_in,
                inf_dec_initial_h,
                inf_target_input
            ],
            outputs=[
                inf_prediction,
                inf_dec_h
            ]
        )
        self.decoder_model.summary(print_fn=logger.info)
    
    def predict(self, document):
        """Make prediction for documents"""
        source_sequence, target_text, source_text = get_single_file_batch(
            self.target_wv,
            self.field,
            self.source_wv,
            document
        )
        logging.info('Making prediction for:{}'.format(source_text))
        enc_out, enc_last_h = self.encoder_model.predict(
            source_sequence
        )
        target_prediction = ''
        #attention_scores = []
        dec_in = numpy.zeros((1,1), dtype='int')
        dec_in[0][0] = self.target_wv.vocab['_bos_'].index
        dec_h = enc_last_h[-1].reshape(
            1,
            Global.HIDDEN_DIMENSIONS
        )
        enc_out = enc_out.reshape(
            1,
            len(enc_out),
            Global.HIDDEN_DIMENSIONS
        )
        for _ in range(Global.MAX_TARGET_LENGTH):
            dec_out, dec_h = self.decoder_model.predict(
                [enc_out, dec_h, dec_in]
            )
            logging.debug('Decoder prediction: {}\n\tfor decoder input: {}'
                          .format(dec_out, dec_in))
            dec_out = numpy.argmax(dec_out)
            word = self.target_wv.index2word[dec_out]
            dec_in[0][0] = dec_out
            if word == '_eos_':
                break
            target_prediction += word + ' '

        logging.info('Prediction: {}'.format(target_prediction))
        logging.info('Target: {}'.format(target_text))

class TransformerModel(EncoderDecoderModel):

    def build_model(self, sequence_length):
        """Build and Fit model"""
        source_embedding_input = Input(shape=(sequence_length,))
        source_embedding = self.source_wv.get_keras_embedding(
            train_embeddings=False
        )
        source_embedded = source_embedding(source_embedding_input)
        enc_positional_encoding = PositionalEncoding()
        enc_pos_encoded = enc_positional_encoding(source_embedded)
        enc_attention_layer = MultiHeadAttention()
        intermediate_1 = enc_attention_layer([
            enc_pos_encoded,
            enc_pos_encoded,
            enc_pos_encoded
        ])
        enc_add_norm_layer_1 = AddAndNorm()
        intermediate_2 = enc_add_norm_layer_1([enc_pos_encoded, intermediate_1])
        enc_feed_forward = FeedForward()
        intermediate_3 = enc_feed_forward(intermediate_2)
        enc_add_norm_layer_2 = AddAndNorm()
        enc_out = enc_add_norm_layer_2([intermediate_2, intermediate_3])
        
        target_embedding_input = Input(
            shape=(None,)
        )
        target_embedding = self.target_wv.get_keras_embedding(
            train_embeddings=False
        )
        target_embedded = target_embedding(target_embedding_input)
        dec_postitional_encoding = PositionalEncoding()
        dec_pos_encoded = dec_postitional_encoding(target_embedded)
        mask_layer = MaskLayer()
        masked_values = mask_layer(dec_pos_encoded)
        dec_attention_layer_1 = MultiHeadAttention()
        intermediate_4 = dec_attention_layer_1([
            dec_pos_encoded,
            dec_pos_encoded,
            masked_values
        ])
        dec_add_norm_layer_1 = AddAndNorm()
        intermediate_5 = dec_add_norm_layer_1([dec_pos_encoded, intermediate_4])
        dec_attention_layer_2 = MultiHeadAttention()
        intermediate_6 = dec_attention_layer_2([
            enc_out,
            enc_out,
            intermediate_5
        ])
        dec_add_norm_layer_2 = AddAndNorm()
        intermediate_7 = dec_add_norm_layer_2([intermediate_5, intermediate_6])
        dec_feed_forward = FeedForward()
        intermediate_8 = dec_feed_forward(intermediate_7)
        dec_add_norm_layer_3 = AddAndNorm()
        intermediate_9 = dec_add_norm_layer_3([intermediate_7, intermediate_8])
        dense = Dense(units=Global.HIDDEN_DIMENSIONS, activation='softmax')
        dec_out = dense(intermediate_9)

        self.model = Model(
            inputs=[source_embedding_input, target_embedding_input],
            outputs=dec_out
        )
        optimizer = SGD(lr=1e-2,
                        momentum=0.9,
                        decay=1e-2/Global.ENCODER_DECODER_EPOCHS)
        self.model.compile(optimizer=optimizer,
                           loss='categorical_crossentropy',
                           metrics=['accuracy'])
        self.model.summary()
        logger = logging.getLogger(__name__)
        self.model.summary(print_fn=logger.info)

        #start still to be done
        batch_size = 1