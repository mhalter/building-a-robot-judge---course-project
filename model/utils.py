"""Utility functions"""

import os
import json
import nltk
import numpy
from zipfile import ZipFile
from pandas import DataFrame
from iterables import WordsIterator
import Global


def get_documents(path):
    """Return a pandas.DataFrame of all the available and usable
    documents
    """
    nltk.download('punkt')
    zip_file_list = []

    for r, _, files in os.walk(path):
        for f in files:
            if '.zip' in f:
                zip_file_list.append(os.path.join(r,f))
    document_list = []
    source_file_list = []

    for f in zip_file_list:
        zip_file = ZipFile(f, mode='r')
        
        for name in zip_file.namelist():
            if '.json' in name and not '__MAXOSX' in name:
                doc = json.loads(zip_file.read(name))
                if (not doc['opinion'] == ''
                        and 'overview' in doc['case_summary']
                        and 'outcome' in doc['case_summary']
                        and not doc['case_summary']['overview'] == ''
                        and not doc['case_summary']['outcome'] == ''
                        and not doc['headnotes'] == ''):
                    document_list.append(name)
                    source_file_list.append(f)
                    joined_headnotes = 'Headnote: '
                    for hn in doc['headnotes']:
                        joined_headnotes = ' Headnote: '.join(
                            [joined_headnotes, doc['headnotes'][hn]]
                        )
    return DataFrame(data={'document_name': document_list,
                           'source_file': source_file_list})


def get_single_file_batch(target_wv,
                          target_field,
                          source_wv,
                          document):
    source_word_iterator = WordsIterator(documents=document,
                                         field=0)
    target_word_iterator = WordsIterator(documents=document,
                                         field=target_field)
    source_text = next(source_word_iterator)
    target_text = next(target_word_iterator)
    encoder_sequence_data = numpy.zeros(
        len(source_text),
        dtype='float32'
    )
    for j, word in enumerate(source_text):
        try:
            encoder_sequence_data[j] = source_wv.vocab[
                word
            ].index
        except KeyError:
            encoder_sequence_data[j] = source_wv.vocab[
                '_unknown_'
            ].index
    return encoder_sequence_data, target_text, source_text

def generate_batch(batch_size,
                   target_wv,
                   target_field,
                   source_wv,
                   documents):
    """Continuously generate batches of size batch_size
    for target_wv, source_wv from documents

    target_field:
        1: ['case_summary']['overview']
        2: ['case_summary']['outcome']
        3: ['headnotes']
    """
    source_word_iterator = WordsIterator(documents=documents,
                                         field=0)
    target_word_iterator = WordsIterator(documents=documents,
                                         field=target_field)

    while True:
        source_text_list = []
        target_text_list = []

        while len(source_text_list) < batch_size:
            try:
                source_text_list.append(next(source_word_iterator))
                target_text_list.append(next(target_word_iterator))
            except StopIteration:
                source_word_iterator = WordsIterator(
                    documents=documents,
                    field=0
                )
                target_word_iterator = WordsIterator(
                    documents=documents,
                    field=target_field
                )
        encoder_sequence_data = numpy.zeros(
            (len(source_text_list), len(max(source_text_list, key=len))),
            dtype='float32'
        )
        decoder_sequence_data = numpy.zeros(
            (len(target_text_list), len(max(target_text_list, key=len))),
            dtype='float32'
        )
        target_sequence_data = numpy.zeros(
            (
                len(target_text_list),
                len(max(target_text_list, key=len)),
                len(target_wv.vocab)
            ),
            dtype='float32'
        )
        for i, (source_text, target_text) in enumerate(
                    zip(source_text_list, target_text_list)
                ):
            for j, word in enumerate(source_text):
                try:
                    encoder_sequence_data[i, j] = source_wv.vocab[
                        word
                    ].index
                except KeyError:
                    encoder_sequence_data[i, j] = source_wv.vocab[
                        '_unknown_'
                    ].index
                
            for j, word in enumerate(target_text):
                try:
                    decoder_sequence_data[i, j] = target_wv.vocab[
                        word
                    ].index
                except KeyError:
                    decoder_sequence_data[i, j] = target_wv.vocab[
                        '_unknown_'
                    ].index
                    
                if j > 0:
                    try:
                        target_sequence_data[
                            i,
                            j-1,
                            target_wv.vocab[word].index
                        ] = 1
                    except KeyError:
                        target_sequence_data[
                            i,
                            j-1,
                            target_wv.vocab['_unknown_'].index
                        ] = 1
        yield ([encoder_sequence_data, decoder_sequence_data],
               target_sequence_data)